package main

import (
	"candidate/gentoken"
	"candidate/job"
	"candidate/jobdetail"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/go-sql-driver/mysql"
	"github.com/golang-jwt/jwt"
	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/spf13/viper"
)

// Secret key used to sign tokens (should be stored in an environment variable or a safer place)
var jwtSecretKey = []byte("1234")

func initViper() {
	viper.AddConfigPath("configs")
	viper.SetConfigName("config")

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("cannot read in viper config:%s", err)
	}

	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
}

func main() {
	initViper()
	e := echo.New()
	maria := newMariaConn()

	defer maria.Close()
	e.GET("/health", makeHealthHandler(maria))

	e.POST("/token", generateToken)

	r := e.Group("/api")
	r.Use(middleware.JWTWithConfig(middleware.JWTConfig{
		SigningKey: []byte("1234"),
	}))
	//r.Use(RoleBasedAuthMiddleware(viper.GetString("role.name")))
	r.POST("/job-interviews", createJobInterview)
	r.GET("/job-interviews/:id", getJobInterview)

	e.Logger.Fatal(e.Start(":1323"))
}

func newMariaConn() *gorm.DB {
	conf := mysql.Config{
		DBName:               viper.GetString("mysql.database.creditcards"),
		User:                 viper.GetString("mysql.username"),
		Passwd:               viper.GetString("mysql.password"),
		Net:                  "tcp",
		Addr:                 viper.GetString("mysql.host") + ":" + viper.GetString("mysql.port"),
		AllowNativePasswords: true,
		Timeout:              viper.GetDuration("mysql.timeout"),
		ReadTimeout:          viper.GetDuration("mysql.readtimeout"),
		WriteTimeout:         viper.GetDuration("mysql.writetimeout"),
		ParseTime:            viper.GetBool("mysql.parsetime"),
		Loc:                  time.Local,
	}

	conn, err := gorm.Open("mysql", conf.FormatDSN())
	if err != nil {
		log.Fatalf("cannot open mysql connection:%s", err)
	}

	conn.DB().SetMaxIdleConns(viper.GetInt("mysql.maxidle"))
	conn.DB().SetMaxOpenConns(viper.GetInt("mysql.maxopen"))
	conn.DB().SetConnMaxLifetime(viper.GetDuration("mysql.maxlifetime"))
	conn.LogMode(viper.GetBool("mysql.debug"))

	return conn
}

func makeHealthHandler(maria *gorm.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		err := maria.DB().Ping()
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{
				"status": "unhealty",
				"msg":    fmt.Sprintf("maria ping error: %s", err.Error()),
			})
		}

		return c.JSON(http.StatusOK, map[string]string{"status": "healthy"})
	}
}

func generateToken(c echo.Context) error {
	var gen gentoken.GenRequest
	if err := c.Bind(&gen); err != nil {
		return err
	}

	// Create token
	token := jwt.New(jwt.SigningMethodHS256)

	// Set claims
	claims := token.Claims.(jwt.MapClaims)
	claims["name"] = gen.Username
	claims["role"] = viper.GetString("role.name")
	claims["exp"] = time.Now().Add(time.Hour * 72).Unix()

	// Generate encoded token
	t, err := token.SignedString(jwtSecretKey)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]string{
		"accessToken": t,
	})
}

func RoleBasedAuthMiddleware(allowedRoles ...string) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			user, ok := c.Get("user").(*jwt.Token)
			if !ok {
				// Handle the error - the value is not a JWT token
				return echo.NewHTTPError(http.StatusUnauthorized, "Invalid token")
			}
			claims := user.Claims.(jwt.MapClaims)
			userRole := getUserRoleFromToken(claims)

			log.Printf("User role from token: %s", userRole)

			for _, role := range allowedRoles {
				log.Printf("Checking allowed role: %s", role)
				if userRole == role {
					return next(c)
				}
			}

			log.Println("Access forbidden: User role not allowed")
			return echo.ErrForbidden
		}
	}
}

// Handlers for the job interview routes
func createJobInterview(c echo.Context) error {
	var req job.JobInterviewRequest

	if err := c.Bind(&req); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	RoleBasedAuthMiddleware(req.Role)

	response := job.JobInterviewResponse{
		// Simulate fetching from a database
		Username:        req.Username,
		ID:              uuid.New().String(),
		Topic:           "Software Engineering",
		Detail:          "Interview for the Software Engineer position",
		Status:          "Scheduled",
		CreatedDateTime: time.Now(),
		CreatedBy:       req.Username,
		UpdatedDateTime: time.Now(),
		UpdatedBy:       req.Username,
	}

	return c.JSON(http.StatusCreated, response)
}

func getJobInterview(c echo.Context) error {

	// Simulate fetching the job interview from a database
	interview := jobdetail.JobInterview{
		ID:          c.Param("id"),
		Topic:       "Software Engineering",
		Detail:      "Interview for the Software Engineer position",
		Status:      "Scheduled",
		CreatedBy:   "TBC",
		Description: "Discussion about technical skills and experience",
		Comments: []jobdetail.Comment{
			{Username: "interviewer1", Detail: "Candidate has strong technical skills.", DateComment: time.Now().Add(-24 * time.Hour)},
			{Username: "interviewer2", Detail: "Needs to improve problem-solving abilities.", DateComment: time.Now().Add(-23 * time.Hour)},
		},
		History: []jobdetail.History{
			{Timestamp: time.Now().Add(-48 * time.Hour), Event: "Interview created"},
			{Timestamp: time.Now().Add(-24 * time.Hour), Event: "Interview details updated"},
		},
	}

	return c.JSON(http.StatusOK, interview)
}

func getUserRoleFromToken(claims jwt.MapClaims) string {
	role, ok := claims["role"].(string)
	if !ok {
		fmt.Println(role)

		return ""
	}

	return role
}
