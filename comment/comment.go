package comment

import "time"

type Comment struct {
	Username    string    `json:"username"`
	Detail      string    `json:"detail"`
	DateComment time.Time `json:"date_comment"`
}
