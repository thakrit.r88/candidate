package job

import "time"

type JobInterviewRequest struct {
	Username string `json:"username"`
	Role     string `json:"role"`
}

type JobInterviewResponse struct {
	Username        string    `json:"username"`
	ID              string    `json:"id"`
	Topic           string    `json:"topic"`
	Detail          string    `json:"detail"`
	Status          string    `json:"status"`
	CreatedBy       string    `json:"created_by"`
	CreatedDateTime time.Time `json:"created_date_time"`
	UpdatedBy       string    `json:"updated_by"`
	UpdatedDateTime time.Time `json:"updated_date_time"`
}
