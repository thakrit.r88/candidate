package jobdetail

import "time"

type JobInterview struct {
	ID          string    `json:"id"`
	Topic       string    `json:"topic"`
	Detail      string    `json:"detail"`
	Status      string    `json:"status"`
	CreatedBy   string    `json:"created_by"`
	Description string    `json:"description"`
	Comments    []Comment `json:"comments"`
	History     []History `json:"history"`
}

type Comment struct {
	Username    string    `json:"username"`
	Detail      string    `json:"detail"`
	DateComment time.Time `json:"date_comment"`
}

type History struct {
	Timestamp time.Time `json:"timestamp"`
	Event     string    `json:"event"`
	ChangedBy string    `json:"changed_by"`
}
