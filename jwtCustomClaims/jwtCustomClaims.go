package jwtCustomClaims

import (
	"github.com/golang-jwt/jwt/v4"
)

type jwtCustomClaims struct {
	User string `json:"user"`
	Role string `json:"role"`
	jwt.RegisteredClaims
}
