FROM golang:1.14.10-alpine3.12
# RUN mkdir /app
# ADD . /app
# WORKDIR /app
# RUN go build -o main . 


# RUN apk update && apk add gcc git 
# WORKDIR /app
# COPY . .
# RUN go build ./...
# RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

# CMD ["./app"]  

RUN apk update && apk add gcc git 
WORKDIR /app
COPY . .
RUN go build ./...
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

FROM alpine:latest  
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=0 /app/app .
CMD ["./app"]  
